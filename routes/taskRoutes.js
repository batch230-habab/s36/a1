const express = require("express");
// express.Router() method allows access to HTTP methods
const router = express.Router();

const taskController = require ("../controllers/taskController");

//create - task routes
router.post("/addTask", taskController.createTaskController);

//get ALL task
router.get("/allTasks", taskController.getAllTaskController);

//delete task
router.delete("/deleteTask/:taskId", taskController.deleteTaskController);

//archive
router.put("/:id/archive", (req, res) => {
	taskController.changeNameToArchive(req.params.id).then(resultFromController => res.send(resultFromController));
});
//update task name
router.patch("/updateTask/:taskId", taskController.updateTaskNameController);

//get task by ID
router.get("/:taskId", taskController.getTaskById);

//update task status completed by id
router.put("/:taskId/complete/", taskController.updateTaskStatusCompleted);

//patch test
router.patch("/:taskId/complete/", taskController.updateTaskStatusCompleted);

module.exports = router;